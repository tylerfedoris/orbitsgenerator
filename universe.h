#ifndef UNIVERSE_H
#define UNIVERSE_H

#include "orbit.h"
#include <list>
#include <array>
#include <vector>
#include <QGraphicsScene>

using std::list;
using std::vector;

class Universe : public QGraphicsScene
{
    Q_OBJECT
public:
    Universe();

    unsigned int a() const;
    void setA(unsigned int a);

    unsigned int b() const;
    void setB(unsigned int b);

    unsigned int c() const;
    void setC(unsigned int c);

    unsigned int n() const;
    void setN(unsigned int n);

    unsigned int k() const;
    void setK(unsigned int k);

    unsigned int modulus() const;
    void setModulus(unsigned int modulus);

    void assignNodeCoordinates(Orbit* orbit);
    void assignOrbitCoordinates(QPointF startPos);

    void sortOrbits();

    void generateUniverse();

    struct CompareOrbitRadius
    {
        bool operator () (Orbit* lhs, Orbit* rhs)
        {
            return lhs->radius() < rhs->radius();
        }
    };

private:
    unsigned int a_;
    unsigned int b_;
    unsigned int c_;
    unsigned int n_;
    unsigned int k_;
    unsigned int modulus_;

    list<Orbit*> orbits_;
    list<unsigned int> elements;
    list<unsigned int> radiiCounts;

    void populateElements();
    unsigned int calculatePolynomial(unsigned int x);
    void calculateModulus();
    QPointF getCoordinates(qreal theta, unsigned int i, qreal radius, QPointF center);
    void addWhisker(Orbit* orbit, unsigned int value, Node* connectingNode);
    void processWhiskerSeries(Orbit* orbit, Node* node, unsigned int anchorIndex, vector<uint> pendingValues);
    Orbit* addOrbit(vector<uint> pendingValues, unsigned int orbitAnchor);
    Node* findInOrbit(Orbit* orbit, unsigned int value);
    void generateOrbit();
};

#endif // UNIVERSE_H
