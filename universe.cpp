#include "universe.h"
#include "findinvector.h"
#include <QPointF>
#include <QtMath>
#include <QDebug>

Universe::Universe()
{
    this->setItemIndexMethod(QGraphicsScene::NoIndex);
    this->a_ = 0;
    this->b_ = 0;
    this->c_ = 0;
    this->n_ = 0;
    this->k_ = 0;
    this->modulus_ = 0;
}

unsigned int Universe::a() const
{
    return a_;
}

void Universe::setA(unsigned int a)
{
    a_ = a;
}

unsigned int Universe::b() const
{
    return b_;
}

void Universe::setB(unsigned int b)
{
    b_ = b;
}

unsigned int Universe::c() const
{
    return c_;
}

void Universe::setC(unsigned int c)
{
    c_ = c;
}

unsigned int Universe::n() const
{
    return n_;
}

void Universe::setN(unsigned int n)
{
    n_ = n;
}

unsigned int Universe::k() const
{
    return k_;
}

void Universe::setK(unsigned int k)
{
    k_ = k;
}

unsigned int Universe::modulus() const
{
    return modulus_;
}

void Universe::setModulus(unsigned int modulus)
{
    modulus_ = modulus;
}

void Universe::populateElements()
{
    for (unsigned int i = 0; i < modulus_; i++)
    {
        elements.push_back(i);
    }

    return;
}

void Universe::calculateModulus()
{
    setModulus(uint(qPow(n_, k_) + 0.5));
}

unsigned int Universe::calculatePolynomial(unsigned int x)
{
    unsigned int xSquared = uint(qPow(x, 2) + 0.5);

    return ( (a() * xSquared) + (b() * x) + c() ) % modulus_; // ax^2 + bx + c mod n^k5
}

QPointF Universe::getCoordinates(qreal theta, unsigned int i, qreal radius, QPointF center)
{
    qreal x = 0;
    qreal y = 0;

    x = radius * cos(theta * i) + center.x();
    y = radius * sin(theta * i) + center.y();

    return QPointF(x, y);
}

void Universe::assignNodeCoordinates(Orbit* orbit)
{
    list<Node*>::iterator it;
    unsigned int i = 1;
    qreal x = 0;
    qreal y = 0;

    for (it = orbit->nodes_.begin(); it != orbit->nodes_.end(); it++)
    {
        //orbit->nodes_.at(i - 1)->setPos(getCoordinates(orbit->theta_, i, orbit->radius_, orbit->boundingRect().center()));
        x = orbit->radius() * cos(orbit->theta_ * i) + orbit->boundingRect().center().x();
        y = orbit->radius() * sin(orbit->theta_ * i) + orbit->boundingRect().center().y();
        (*it)->setPos(x, y);
        (*it)->valueText_->setPlainText(QString::number((*it)->value_)); // <------------------------------
        i++;
    }
}

void Universe::assignOrbitCoordinates(QPointF startPos)
{
    list<Orbit*>::iterator it;
    list<unsigned int>::iterator radii_it = radiiCounts.begin();
    Orbit* lastOrbit = nullptr;
    Orbit* firstOrbitInRow = nullptr;
    qreal offset = 0;
    unsigned int count = 0;

    for(it = orbits_.begin(); it != orbits_.end(); it++)
    {
        if (it == orbits_.begin())
        {
            (*it)->setPos(startPos);
            lastOrbit = *it;
            firstOrbitInRow = *it;
            count++;
        }
        else
        {
            if (count % 3 == 0)
            {
                QPointF newCoordinates(firstOrbitInRow->pos().x(), firstOrbitInRow->pos().y() + (firstOrbitInRow->radius() * 3));
                (*it)->setPos(newCoordinates);
                lastOrbit = *it;
                firstOrbitInRow = *it;
                count++;
            }
            else
            {
                QPointF newCoordinates(lastOrbit->pos().x() + (lastOrbit->radius() * 3), lastOrbit->pos().y());
                (*it)->setPos(newCoordinates);
                lastOrbit = *it;
                count++;
            }
        }
    }

    lastOrbit = nullptr;
    firstOrbitInRow = nullptr;
    delete lastOrbit;
    delete firstOrbitInRow;
}

void Universe::sortOrbits()
{
    this->orbits_.sort(CompareOrbitRadius());
    this->orbits_.reverse();

    list<Orbit*>::iterator it = this->orbits_.begin();
    qreal currentRadius = (*it)->radius();
    it++;
    unsigned int count = 1;

    for(; it != this->orbits_.end(); it++)
    {
        if (!qFuzzyCompare((*it)->radius(), currentRadius))
        {
            this->radiiCounts.push_back(count);
            currentRadius = (*it)->radius();
            count = 1;
        }
        else
        {
            count++;
        }
    }

    this->radiiCounts.push_back(count);
}

void Universe::addWhisker(Orbit* orbit, unsigned int value, Node* connectingNode)
{
    connectingNode->whiskers_.push_back(nullptr);
    connectingNode->whiskers_.back() = new Node(orbit);
    connectingNode->whiskers_.back()->value_ = value;
    connectingNode->whiskers_.back()->valueText_->setPlainText(QString::number(connectingNode->whiskers_.front()->value_));
    connectingNode->whiskers_.back()->bIsWhisker_ = true;

    connectingNode->whiskers_.back()->nextNode_ = connectingNode;

    connectingNode->whiskers_.back()->layer_ = connectingNode->layer_ + 1;

    elements.remove(value);
}

void Universe::processWhiskerSeries(Orbit* orbit, Node* node, unsigned int anchorIndex, vector<uint> pendingValues)
{
    Node* connectingWhisker = nullptr;
    Node* existingNode = nullptr;

    for(unsigned int i = anchorIndex; i > 0; i--)
    {
        existingNode = findInOrbit(orbit, pendingValues[i - 1]);

        if (i == anchorIndex && !existingNode)
        {
            addWhisker(orbit, pendingValues[i - 1], node);
            connectingWhisker = node->whiskers_.back();
        }
        else if (!existingNode && connectingWhisker)
        {
            addWhisker(orbit, pendingValues[i - 1], connectingWhisker);
            connectingWhisker = connectingWhisker->whiskers_.back();
        }
        else if (existingNode && existingNode->bIsWhisker_)
        {
            connectingWhisker = existingNode;
            existingNode = nullptr;
        }
    }

    connectingWhisker = nullptr;
    existingNode = nullptr;
    delete connectingWhisker;
    delete existingNode;
}

Orbit* Universe::addOrbit(vector<uint> pendingValues, unsigned int orbitAnchor)
{

    bool anchorFound = false;
    Orbit* orbit = new Orbit();

    for(unsigned int i = 0; i < pendingValues.size(); i++)
    {
        if (pendingValues[i] == orbitAnchor && !anchorFound)
        {
            anchorFound = true;
            orbit->nodes_.push_back(nullptr);
            orbit->nodes_.back() = new Node(orbit);
            orbit->nodes_.back()->value_ = pendingValues[i];

            elements.remove(pendingValues[i]);

            orbit->nodes_.back()->layer_ = 0;
            orbit->nodes_.back()->bIsAnchor_ = true;

            if (i != 0)
            {
                processWhiskerSeries(orbit, orbit->nodes_.back(), i, pendingValues);
            }
        }
        else if (anchorFound)
        {
            orbit->nodes_.push_back(nullptr);
            orbit->nodes_.back() = new Node(orbit);
            orbit->nodes_.back()->value_ = pendingValues[i];

            elements.remove(pendingValues[i]);

            orbit->nodes_.back()->layer_ = 0;

            list<Node*>::iterator prevNode = orbit->nodes_.end();
            prevNode--;
            orbit->nodes_.back()->prevNode_ = *prevNode;
            *prevNode = orbit->nodes_.back();

            if (i == pendingValues.size() - 1)
            {
                orbit->nodes_.back()->nextNode_ = orbit->nodes_.front();
                orbit->nodes_.front()->prevNode_ = orbit->nodes_.back();
            }
        }
    }

    orbit->theta_ = (2 * M_PI) / orbit->nodes_.size();
    orbit->updateRadius();

    return orbit;
}

void Universe::generateUniverse()
{
    static bool firstTime = true;
    list<Orbit*>::iterator orbit_it;
    list<Node*>::iterator node_it;
    list<Node*>::iterator whisker_it;

    Node* existingNode = nullptr;

    this->calculateModulus();
    this->populateElements();

    while (elements.size() > 0)
    {
        if(firstTime)
        {
            this->generateOrbit();
            firstTime = false;
        }
        else
        {
            for(orbit_it = orbits_.begin(); orbit_it != orbits_.end() && !existingNode; orbit_it++)
            {
                existingNode = findInOrbit(*orbit_it, calculatePolynomial(elements.front()));

                if(existingNode)
                {
                    addWhisker(*orbit_it, elements.front(), existingNode);
                }
            }

            if(!existingNode)
            {
                this->generateOrbit();
            }
        }

        existingNode = nullptr;
    }

    sortOrbits();

    assignOrbitCoordinates(QPointF(0, 0));

    for(orbit_it = orbits_.begin(); orbit_it != orbits_.end(); orbit_it++)
    {
        this->addItem(*orbit_it);
        assignNodeCoordinates(*orbit_it);
    }

    delete existingNode;
}

Node* Universe::findInOrbit(Orbit* orbit, unsigned int value)
{
    list<Node*>::iterator node_it;
    list<Node*>::iterator whisker_it;

    for(node_it = orbit->nodes_.begin(); node_it != orbit->nodes_.end(); node_it++)
    {
        if(value == (*node_it)->value_)
        {
            return *node_it;
        }

        for(whisker_it = (*node_it)->whiskers_.begin(); whisker_it != (*node_it)->whiskers_.end(); whisker_it++)
        {
            if(value == (*whisker_it)->value_)
            {
                return *whisker_it;
            }
        }
    }

    return nullptr;
}

void Universe::generateOrbit()
{
    list<Orbit*>::iterator orbit_it;
    Node* existingNode = nullptr;
    unsigned int orbitAnchor = modulus_ + 1;
    vector<uint> pendingValues;
    unsigned int pendingValue = elements.front();
    std::pair<bool, int> duplicateNode = findInVector<uint>(pendingValues, pendingValue);

    while(!duplicateNode.first && !existingNode)
    {
        pendingValues.push_back(pendingValue);
        pendingValue = calculatePolynomial(pendingValue);
        duplicateNode = findInVector<uint>(pendingValues, pendingValue);

        if(duplicateNode.first)
        {
            orbitAnchor = pendingValue;
        }

        if(this->orbits_.size() > 0)
        {
            for(orbit_it = this->orbits_.begin(); orbit_it != this->orbits_.end() && !existingNode; orbit_it++)
            {
                existingNode = findInOrbit(*orbit_it, pendingValue);

                if(existingNode && !existingNode->bIsWhisker_)
                {
                    processWhiskerSeries(*orbit_it, existingNode, pendingValues.size(), pendingValues);
                }
                else
                {
                    existingNode = nullptr;
                }
            }
        }
    }

    if(!existingNode)
    {
        this->orbits_.push_back(addOrbit(pendingValues, orbitAnchor));
    }

    existingNode = nullptr;
    delete existingNode;
}
