#ifndef LOGWINDOW_H
#define LOGWINDOW_H

#include <QPlainTextEdit>

class LogWindow : public QPlainTextEdit
{
public:
    LogWindow(QWidget *parent = nullptr);
    void appendMessage(const QString& text);
};

#endif // LOGWINDOW_H
