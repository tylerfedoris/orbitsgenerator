#include "universeview.h"

UniverseView::UniverseView(QWidget* parent)  : QGraphicsView(parent)
{
    setBackgroundBrush(Qt::transparent);
}

void UniverseView::wheelEvent(QWheelEvent *event)
{
    setTransformationAnchor(QGraphicsView::AnchorUnderMouse);

    double scaleFactor = 1.15;

    if(event->delta() > 0)
    {
        scale(scaleFactor, scaleFactor);
        this->scene()->update();
    }
    else
    {
        scale(1 / scaleFactor, 1 / scaleFactor);
        this->scene()->update();
    }
}
