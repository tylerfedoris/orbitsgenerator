#include "nodegraphicsitem.h"
#include <cmath>

NodeGraphicsItem::NodeGraphicsItem(QGraphicsItem *parent, int n, int i)
{
    this->setParentItem(parent);
    //setFlag(ItemIsMovable);
    this->n = n;
    this->i = i;
    determinePos();
}

QRectF NodeGraphicsItem::boundingRect() const
{
    return QRectF(-5 - 1, -5 - 1, 10 + 2, 10 + 2);
}

void NodeGraphicsItem::determinePos()
{
    qreal x = 0;
    qreal y = 0;

    qreal radius = parentItem()->boundingRect().width() / 2;
    QPointF parentCenter = this->parentItem()->boundingRect().center();

    x = radius * cos(((2 * M_PI) / n) * i) + parentCenter.x();
    y = radius * sin(((2 * M_PI) / n) * i) + parentCenter.y();

    this->setPos(x, y);
}

void NodeGraphicsItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    QRectF rec = boundingRect();
    QBrush brush(Qt::black);
    QPen pen(Qt::black);
    pen.setWidth(2);

    painter->setBrush(brush);
    painter->setPen(pen);

    painter->drawEllipse(rec);
}

