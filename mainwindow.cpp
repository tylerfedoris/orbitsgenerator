#include "mainwindow.h"
#include "universewindow.h"
#include "ui_mainwindow.h"
#include "universe.h"
#include "logwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->labelXSquared->setText("x<sup>2</sup>");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btnGenerate_clicked()
{
    Universe *universe = new Universe();

    this->hide();
    UniverseWindow* uw = new UniverseWindow(nullptr, universe);
    universe->setParent(uw);
    uw->show();

    universe->setA(ui->inputA->text().toUInt());
    universe->setB(ui->inputB->text().toUInt());
    universe->setC(ui->inputC->text().toUInt());
    universe->setN(ui->inputN->text().toUInt());
    universe->setK(ui->inputK->text().toUInt());
    universe->generateUniverse();

    //universe->update();
}
