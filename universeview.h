#ifndef UNIVERSEVIEW_H
#define UNIVERSEVIEW_H

#include <QGraphicsView>
#include <QWheelEvent>

class UniverseView : public QGraphicsView
{
public:
    UniverseView(QWidget *parent = nullptr);

protected:
    virtual void wheelEvent(QWheelEvent *event);
};

#endif // UNIVERSEVIEW_H
