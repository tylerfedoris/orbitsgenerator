#include "node.h"
#include <cmath>

Node::Node(QGraphicsItem *parent)
{
    this->setCacheMode(QGraphicsItem::ItemCoordinateCache);
    this->setParentItem(parent);
    this->value_ = 0;
    this->layer_ = 0;
    this->bIsWhisker_ = false;
    this->bIsAnchor_ = false;
    this->bInOrbit_ = false;
    this->prevNode_ = nullptr;
    this->nextNode_ = nullptr;

    valueText_ = new QGraphicsTextItem();
    valueText_->setParentItem(this);

    //setFlag(ItemIsMovable);
    //determinePos();
}

/*
Node::Node(const Node &node) : QGraphicsItem(node)
{
    this->setParentItem(node.parentItem());
    this->value_ = node.value_;
    this->layer_ = node.layer_;
    this->coordinates_ = node.coordinates_;
    this->bIsWhisker_ = node.bIsWhisker_;
    this->bIsAnchor_ = node.bIsAnchor_;
    this->prevNode_ = node.prevNode_;
    this->nextNode_ = node.nextNode_;
    this->n = node.n;
    this->i = node.i;
}
*/

QRectF Node::boundingRect() const
{
    return QRectF(-5 - 1, -5 - 1, 10 + 2, 10 + 2);
}

void Node::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    QBrush brush;

    if(this->bIsWhisker_)
    {
        this->hide();
    }

    QRectF rec = boundingRect();

    if(this->bIsAnchor_)
    {
        brush = Qt::white;
    }
    else
    {
        brush = Qt::black;
    }

    QPen pen(Qt::black);
    pen.setWidth(2);

    painter->setRenderHint(QPainter::Antialiasing, true);
    painter->setBrush(brush);
    painter->setPen(pen);

    painter->drawEllipse(rec);
}
