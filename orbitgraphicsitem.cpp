#include "orbitgraphicsitem.h"
#include "nodegraphicsitem.h"
#include <cmath>

OrbitGraphicsItem::OrbitGraphicsItem()
{
    setFlag(ItemIsMovable);
    populateNodes();
}

QRectF OrbitGraphicsItem::boundingRect() const
{
    return QRectF(0 - 1, 0 - 1, 400 + 2, 400 + 2);
}

void OrbitGraphicsItem::populateNodes()
{
    int n = 25;

    for(int i = 1; i <= n; i++)
    {
        NodeGraphicsItem *node = new NodeGraphicsItem(this, n, i);
        node = nullptr;
        delete node;
    }
}

void OrbitGraphicsItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    QRectF rec = boundingRect();
    QBrush brush(Qt::white);
    QPen pen(Qt::black);
    pen.setWidth(2);

    painter->setBrush(brush);
    painter->setPen(pen);
    painter->drawEllipse(rec);
}
