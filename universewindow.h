#ifndef UNIVERSEWINDOW_H
#define UNIVERSEWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QKeyEvent>

namespace Ui {
class UniverseWindow;
}

class UniverseWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit UniverseWindow(QWidget *parent = nullptr, QGraphicsScene *scene = nullptr);
    ~UniverseWindow();

private:
    Ui::UniverseWindow *ui;

    void keyPressEvent( QKeyEvent * event );
    void keyReleaseEvent( QKeyEvent * event );
};

#endif // UNIVERSEWINDOW_H
