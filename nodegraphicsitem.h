#ifndef NODEGRAPHICSITEM_H
#define NODEGRAPHICSITEM_H

#include <QPainter>
#include <QGraphicsItem>
#include <QDebug>

class NodeGraphicsItem: public QGraphicsItem
{
public:
    NodeGraphicsItem(QGraphicsItem *parent = nullptr, int n = 0, int i = 0);

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

private:
    void determinePos();
    int n;
    int i;
};

#endif // NODEGRAPHICSITEM_H
