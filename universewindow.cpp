#include "universewindow.h"
#include "ui_universewindow.h"

UniverseWindow::UniverseWindow(QWidget *parent, QGraphicsScene *scene) :
    QMainWindow(parent),
    ui(new Ui::UniverseWindow)
{
    ui->setupUi(this);
    ui->universeView->setScene(scene);
    ui->universeView->setDragMode(QGraphicsView::RubberBandDrag);
    this->showMaximized();
}

UniverseWindow::~UniverseWindow()
{
    delete ui;
}

void UniverseWindow::keyPressEvent( QKeyEvent * event )
{
  if( event->key() == Qt::Key_Space )
  {
    ui->universeView->setDragMode(QGraphicsView::ScrollHandDrag);
  }

  QMainWindow::keyPressEvent(event);

}

void UniverseWindow::keyReleaseEvent( QKeyEvent * event )
{
  if( event->key() == Qt::Key_Space)
  {
    ui->universeView->setDragMode(QGraphicsView::RubberBandDrag);
  }

 QMainWindow::keyReleaseEvent(event);

}
