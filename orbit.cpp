#include "orbit.h"

Orbit::Orbit()
{
    this->setCacheMode(QGraphicsItem::ItemCoordinateCache);
    this->radius_ = 15.91549431;
    this->theta_ = 0;
    setFlag(ItemIsMovable);
    setFlag(ItemIsSelectable);
}

/*
Orbit::Orbit(const Orbit &orbit)
{
    this->radius_ = orbit.radius_;
    this->theta_ = orbit.theta_;

    for(unsigned int i = 0; i < orbit.nodes_.size(); i++)
    {
        this->nodes_.push_back(orbit.nodes_[i]);
    }

    for(unsigned int i = 0; i < orbit.whiskers_.size(); i++)
    {
        this->whiskers_.push_back(orbit.whiskers_[i]);
    }

    this->coordinates_ = orbit.coordinates_;
}
*/

QRectF Orbit::boundingRect() const
{
    return QRectF(0 - 1, 0 - 1, (radius_ * 2) + 2, (radius_ * 2) + 2);
}

void Orbit::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    QRectF rec = boundingRect();
    QBrush brush(Qt::transparent);
    QPen pen(Qt::black);
    pen.setWidth(2);
    pen.setCosmetic(true);

    painter->setRenderHint(QPainter::Antialiasing, true);
    painter->setBrush(brush);
    painter->setPen(pen);
    painter->drawEllipse(rec);

    if(this->isSelected())
    {
        pen = QPen(Qt::darkGray, 1, Qt::DashDotDotLine);
        brush = Qt::NoBrush;
        painter->setPen(pen);
        painter->setBrush(brush);
        painter->drawRect(this->boundingRect());
    }
}

qreal Orbit::radius() const
{
    return this->radius_;
}

void Orbit::updateRadius()
{
    qreal newRadius = (75 * this->nodes_.size()) / (2 * M_PI);

    if (!qFuzzyCompare(this->radius_, newRadius))
    {
        this->prepareGeometryChange();
        this->radius_ = newRadius;
    }
}
