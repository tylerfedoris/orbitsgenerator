#ifndef ORBITGRAPHICSITEM_H
#define ORBITGRAPHICSITEM_H

#include <QPainter>
#include <QGraphicsItem>
#include <QDebug>

class OrbitGraphicsItem : public QGraphicsItem
{
public:
    OrbitGraphicsItem();

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

private:
    void populateNodes();

};

#endif // ORBITGRAPHICSITEM_H
