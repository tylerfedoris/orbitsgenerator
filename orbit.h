#ifndef ORBIT_H
#define ORBIT_H

#include "node.h"
#include <list>
#include <QPainter>
#include <QGraphicsItem>
#include <QDebug>
#include <cmath>

using std::list;

class Orbit : public QGraphicsItem
{
public:
    Orbit();

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    qreal radius() const;
    void updateRadius();

    qreal theta_;
    list<Node*> nodes_;
    list<Node*> whiskers_;

private:
    qreal radius_;
};

#endif // ORBIT_H
