#ifndef NODE_H
#define NODE_H

#include <QPointF>
#include <QPainter>
#include <QGraphicsItem>
#include <QDebug>
#include <list>

using std::list;

class Node : public QGraphicsItem
{
public:
    Node(QGraphicsItem *parent = nullptr);

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    QGraphicsTextItem* valueText_;
    unsigned int value_;
    unsigned int layer_;
    //QPointF coordinates_;
    bool bIsWhisker_;
    bool bIsAnchor_;
    bool bInOrbit_;
    Node* prevNode_;
    Node* nextNode_;
    list<Node*> whiskers_;

private:
    void determinePos();
};

#endif // NODE_H
